<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Bootstrap_to_Wordpress
 */

?>

<?php wp_footer(); ?>

<!-- SIGN UP -->
<section id="signup">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 offset-xl-3">
                <h2>Are you ready to take your coding skill to the <strong>next level</strong>?</h2>

                <p><button class="btn btn-lg btn-block btn-success">Yes, sign me up!</button></p>
            </div>
        </div>
    </div>
</section>

<!-- FOOTER -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <p>
                    <a href="/">
                        <img src="<?php bloginfo('stylesheet_directory') ?>/assets/img/logo.png" alt="B2W">
                    </a>
                </p>
            </div>

            <div class="col-sm-6">
                <?php
                wp_nav_menu([
                    'theme_location' => 'footer',
                    'depth' => 1, // 1 = no dropdowns, 2 = with dropdowns.
                    'container' => 'nav',
                    'container_id' => 'bs-example-navbar-collapse-1',
                    'menu_class' => 'list-unstyled list-inline',
                    'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                    'walker' => new WP_Bootstrap_Navwalker(),
                ]);
                ?>

                <!-- <nav>
                    <ul class="list-unstyled list-inline">
                        <li class="list-inline-item"><a href="">Home</a></li>
                        <li class="list-inline-item"><a href="">Blog</a></li>
                        <li class="list-inline-item"><a href="">Resources</a></li>
                        <li class="list-inline-item"><a href="">Contact</a></li>
                        <li class="list-inline-item signup-link"><a href="">Sign up now</a></li>
                    </ul>
                </nav> -->
            </div>

            <div class="col-sm-3">
                <p>&copy; <?php echo date('Y'); ?> Brad Hussey </p>
            </div>
        </div>

    </div>
</footer>

<!-- MODAL -->
<div class="modal fade" id="subscribeModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Subscribe to our Mailing List</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <p>Simply enter your name and email! As a thank you for joining us, we're going to give you one of
                    our best-selling courses, <em>for free!</em></p>

                <form role="form">
                    <div class="form-group">
                        <label for="subscribe-name" class="sr-only">Your first name</label>
                        <input type="text" class="form-control" id="subscribe-name" placeholder="Your first name">
                    </div>

                    <div class="form-group">
                        <label for="subscribe-email" class="sr-only">Your email</label>
                        <input type="text" class="form-control" id="subscribe-email" placeholder="and your email">
                    </div>

                    <input type="submit" value="Subscribe!" class="btn btn-danger">
                </form>

                <hr>

                <p><small>
                        By providing your email you consent to receiving occational promotional emails &
                        newsletters. <br> No spam. Just good stuff. We respect your privacy & you may unsubscribe at
                        any time.
                    </small></p>
            </div>
        </div>
    </div>
</div>


<!-- BOOTSRAP CORE JS -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

<script src="<?php bloginfo('template_directory') ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_directory') ?>/assets/js/main.js"></script>

</body>

</html>