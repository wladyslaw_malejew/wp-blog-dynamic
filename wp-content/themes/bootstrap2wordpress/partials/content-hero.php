<?php
$prelaunch_price = get_field('prelaunch_price');
$launch_price = get_field('launch_price');
$final_price = get_field('final_price');
$course_url = get_field('course_url');
$button_text = get_field('button_text');
?>

<!-- HERO -->
<section id="hero">
    <article>
        <div class="container">
            <div class="row">

                <div class="col-xl-5">
                    <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/logo-badge.png" alt="bootstrap to Wordpress" class="logo">
                </div>

                <div class="col-xl-7 hero-text">
                    <h1> <?php bloginfo('name') ?> </h1>
                    <p class="lead"> <?php bloginfo('description') ?> </p>

                    <div id="price-timeline">
                        <div class="active price">
                            <h4>Pre-Launch Price <small>Ends soon!</small> </h4>

                            <span> <?php echo $prelaunch_price; ?> </span>
                        </div>

                        <div class="price">
                            <h4>Launch Price <small>Coming soon!</small> </h4>

                            <span> <?php echo $launch_price; ?> </span>
                        </div>

                        <div class="price">
                            <h4>Final Price <br /> <small>Coming soon!</small> </h4>

                            <span> <?php echo $final_price; ?> </span>
                        </div>
                    </div>

                    <p><a class="btn btn-lg btn-danger" target="_blank" href="<?php echo $course_url; ?>"><?php echo $button_text; ?></a></p>
                </div>

            </div>
        </div>
    </article>
</section>