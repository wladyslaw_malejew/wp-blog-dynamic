<!-- VIDEO FEATURETTE -->
<section id="featurette">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 offset-xl-2">
                <h2>Watch the Course Introduction</h2>
                <iframe width="100%" height="415" src="https://www.youtube.com/embed/q-mJJsnOHew" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>