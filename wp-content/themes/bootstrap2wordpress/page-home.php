<?php
/*
Template Name: Home Page
*/

// Advanced Custom Fields 
/* <?php echo $var ?> */
// $prelaunch_price = get_field('prelaunch_price');
// $launch_price = get_field('launch_price');
// $final_price = get_field('final_price');
// $course_url = get_field('course_url');
// $button_text = get_field('button_text');

// $optin_text = get_field('optin_text');
// $optin_button_text = get_field('optin_button_text');

// $income_feature_image = get_field('income_feature_image');
// $income_section_title = get_field('income_section_title');
// $income_section_decription = get_field('income_section_decription');
// $reason_1_title = get_field('reason_1_title');
// $reason_1_description = get_field('reason_1_description');
// $reason_2_title = get_field('reason_2_title');
// $reason_2_description = get_field('reason_2_description');

// $who_section_image = get_field('who_section_image');
// $who_section_title = get_field('who_section_title');
// $who_section_body = get_field('who_section_body');

// $features_section_image = get_field('features_section_image');
// $features_section_title = get_field('features_section_title');

// $project_features_title = get_field('project_features_title');
// $project_features_body = get_field('project_features_body');

// $instructor_section_title = get_field('instructor_section_title');
// $instructor_name = get_field('instructor_name');
// $bio_excerpt = get_field('bio_excerpt');
// $full_bio = get_field('full_bio');
// $twitter_username = get_field('twitter_username');
// $facebook_username = get_field('facebook_username');
// $number_of_students = get_field('number_of_students');
// $number_of_reviews = get_field('number_of_reviews');
// $number_of_courses = get_field('number_of_courses');


get_header(); ?>

<?php get_template_part('partials/content', 'hero'); ?>

<?php get_template_part('partials/content', 'optin'); ?>

<?php get_template_part('partials/content', 'boost'); ?>

<?php get_template_part('partials/content', 'benefits'); ?>

<?php get_template_part('partials/content', 'course-features'); ?>

<?php get_template_part('partials/content', 'project-features'); ?>

<?php get_template_part('partials/content', 'featurette'); ?>

<?php get_template_part('partials/content', 'instructor'); ?>

<?php get_template_part('partials/content', 'testimonials'); ?>

<?php get_footer(); ?>